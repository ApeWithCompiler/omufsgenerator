package omufsgenerator

import (
	"crypto/sha1"
	"encoding/hex"
	"github.com/google/uuid"
)

type NoneGenerator struct {}

func NewNoneGenerator() Generator {
	return &NoneGenerator{}
}

func (g *NoneGenerator) GenerateValue(key string) (string, error) {
	return key, nil
}

type Sha1Generator struct {}

func NewSha1Generator() Generator {
	return &Sha1Generator{}
}

func (g *Sha1Generator) GenerateValue(key string) (string, error) {
	h := sha1.New()
	h.Write([]byte(key))
	return hex.EncodeToString(h.Sum(nil)), nil
}

type UUIDGenerator struct {}

func NewUUIDGenerator() Generator {
	return &UUIDGenerator{}
}

func (g *UUIDGenerator) GenerateValue(key string) (string, error) {
	id := uuid.New()
	return id.String(), nil
}
