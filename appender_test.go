package omufsgenerator

import (
	"testing"
)

func TestStaticExtensionAppender_AppendPostfix(t *testing.T) {
	sut := NewAppender(APPENDER_POSTFIX, NewStaticAppenderValue("baz"))

	want := "foobarbaz"
	got, _ := sut.Append("foobar", "")

	if got != want {
		t.Errorf("Append() returned %v, want %v", got, want)
	}
}

func TestStaticExtensionAppender_AppendPrefix(t *testing.T) {
	sut := NewAppender(APPENDER_PREFIX, NewStaticAppenderValue("baz"))

	want := "bazfoobar"
	got, _ := sut.Append("foobar", "")

	if got != want {
		t.Errorf("Append() returned %v, want %v", got, want)
	}
}

func TestReferenceAppender_AppendSingleExt(t *testing.T) {
	sut := NewAppender(APPENDER_POSTFIX, NewReferenceAppenderValue())

	want := "foobar.baz"
	got, _ := sut.Append("foobar", "test.baz")

	if got != want {
		t.Errorf("AppendExtension() returned %v, want %v", got, want)
	}
}
