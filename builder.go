package omufsgenerator

import ()

// Generator controlls how the value is to be generated
type Generator interface {
	GenerateValue(key string) (string, error)
}

// ValueSplitter splits the value into segments
type ValueSplitter interface {
	Split(inp string) (string, error)
}

type FilepathGenerator struct {
	Generator Generator
	Splitter ValueSplitter
	Appender []Appender
}

func (g *FilepathGenerator) GenerateValue(key string) (string, error) {
	p, err := g.Generator.GenerateValue(key)
	if err != nil {
		return "", err
	}

	s, err := g.Splitter.Split(p)
	if err != nil {
		return "", err
	}

	for i := range g.Appender {
		s, err = g.Appender[i].Append(s, key)
		if err != nil {
			return "", err
		}
	}

	return s, nil
}

type GeneratorBuilder struct {
	Generator Generator
	Splitter ValueSplitter
	Appender []Appender
}

func NewGeneratorBuilder() *GeneratorBuilder {
	return &GeneratorBuilder{
		Generator: NewNoneGenerator(),
		Splitter: NewNoneSplitter(),
	}
}

func (gb *GeneratorBuilder) SetGenerator(generator Generator) *GeneratorBuilder {
	gb.Generator = generator
	return gb
}

func (gb *GeneratorBuilder) SetSplitter(splitter ValueSplitter) *GeneratorBuilder {
	gb.Splitter = splitter
	return gb
}

func (gb *GeneratorBuilder) AddAppender(appender Appender) *GeneratorBuilder {
	gb.Appender = append(gb.Appender, appender)
	return gb
}

func (gb *GeneratorBuilder) Build() FilepathGenerator {
	return FilepathGenerator{
		Generator: gb.Generator,
		Splitter: gb.Splitter,
		Appender: gb.Appender,
	}
}
