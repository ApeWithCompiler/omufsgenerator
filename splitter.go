package omufsgenerator

import (
	"errors"
	"strings"
)

type NoneSplitter struct {}

func NewNoneSplitter() ValueSplitter {
	return &NoneSplitter{}
}

func (sp *NoneSplitter) Split(key string) (string, error) {
	return key, nil
}

type StaticSegmentSplitter struct {
	Depth int
	SegmentSize int
}

func NewStaticSegmentSplitter(depth, segmentsize int) ValueSplitter {
	return &StaticSegmentSplitter{
		Depth: depth,
		SegmentSize: segmentsize,
	}
}

func (sp *StaticSegmentSplitter) Split(key string) (string, error) {
	if len(key) < sp.Depth * sp.SegmentSize + 1 {
		return "", errors.New("Key to short")
	}

	var segments []string
	
	var i int
	for i = 0; i < sp.Depth; i++ {
		s := key[sp.SegmentSize * i: sp.SegmentSize * (i + 1)]
		segments = append(segments, s)
	}
	segments = append(segments, key[sp.SegmentSize * i:])

	return strings.Join(segments, "/"), nil
}
