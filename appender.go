package omufsgenerator

import (
	"fmt"
	"path"
)

const (
	APPENDER_POSTFIX int8 = 0
	APPENDER_PREFIX = 1
)

type AppenderValue interface {
	GetValue(ref string) (string, error)
}

// StaticAppenderValue appends static extension to all input
type StaticAppenderValue struct {
	val string
}

// Allow for extensions with and without prefix dot
func NewStaticAppenderValue(val string) AppenderValue {
	return &StaticAppenderValue{
		val: val,
	}
}

func (av *StaticAppenderValue) GetValue(ref string) (string, error) {
	return av.val, nil
}

// ReferenceAppenderValue takes extension from reference input
type ReferenceAppenderValue struct {}

func NewReferenceAppenderValue() AppenderValue {
	return &ReferenceAppenderValue{}
}

func (av *ReferenceAppenderValue) GetValue(ref string) (string, error) {
	return path.Ext(ref), nil
}


// StaticAppenderValue appends static extension to all input
type Appender struct {
	Mode int8
	Value AppenderValue
}

// Allow for extensions with and without prefix dot
func NewAppender(mode int8, value AppenderValue) Appender {
	return Appender{
		Mode: mode,
		Value: value,
	}
}

func (sa *Appender) Append(a, b string) (string, error) {
	v, err := sa.Value.GetValue(b)
	if err != nil {
		return "", err
	}

	switch sa.Mode {
	case APPENDER_POSTFIX:
		return a + v, nil	
	case APPENDER_PREFIX:
		return v + a, nil	
	}

	return "", fmt.Errorf("Invalid appender mode")
}
