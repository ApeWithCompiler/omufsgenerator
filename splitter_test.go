package omufsgenerator

import (
	"testing"
)

func TestSplitKeyToDepth(t *testing.T) {
	sut := NewStaticSegmentSplitter(3, 2)

	want := "01/23/45/6789"
	got,_ := sut.Split("0123456789")

	if got != want {
		t.Errorf("Split() returned %v, want %v", got, want)
	}
}

func TestSplitKeyToDepth_KeyToShort(t *testing.T) {
	sut := NewStaticSegmentSplitter(5, 2)

	want := "Key to short"
	_,got := sut.Split("0123456789") // Key to short for 5 segments, 2 chars each

	if got.Error() != want {
		t.Errorf("Split() returned error %v, want %v", got, want)
	}
}
