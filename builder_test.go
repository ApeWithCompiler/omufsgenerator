package omufsgenerator

import (
	"testing"
)

func TestBuilder(t *testing.T) {
	sut := NewGeneratorBuilder().Build()

	want := "foobar"
	got, _ := sut.GenerateValue("foobar")

	if got != want {
		t.Errorf("GenerateValue() returned %v, want %v", got, want)
	}
}

func TestBuilder_Splitter(t *testing.T) {
	sut := NewGeneratorBuilder().SetSplitter(NewStaticSegmentSplitter(2,2)).Build()

	want := "fo/ob/arbaz"
	got, _ := sut.GenerateValue("foobarbaz")

	if got != want {
		t.Errorf("GenerateValue() returned %v, want %v", got, want)
	}
}

func TestBuilder_Appender(t *testing.T) {
	sut := NewGeneratorBuilder().AddAppender(NewAppender(APPENDER_PREFIX, NewStaticAppenderValue("test/"))).Build()

	want := "test/foobar"
	got, _ := sut.GenerateValue("foobar")

	if got != want {
		t.Errorf("GenerateValue() returned %v, want %v", got, want)
	}
}
